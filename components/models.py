__author__ = 'Kewei Duan'

from django.db import models


class Component(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50, blank=False)
    description = models.TextField(blank=True)
    component_family = models.CharField(max_length=200, blank=True)
    license_type = models.CharField(max_length=50, blank=True)
    content_type = models.CharField(max_length=100, blank=True)
    content_uri = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    version = models.IntegerField(blank=False)
    resource = models.CharField(max_length=200, blank=True, default='')
    content = models.TextField(blank=False, default='')

    def __str__(self):
        return str(self.id)

    class Meta:
        managed = True

class Family(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50, blank=False)
    description = models.TextField(blank=True)
    component_profile = models.CharField(max_length=200, blank=True)
    content_uri = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    license_type = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        managed = True


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50, blank=False)
    filename = models.CharField(max_length=50, blank=False)
    description = models.TextField(blank=True)
    content_type = models.CharField(max_length=100, blank=True)
    license_type = models.CharField(max_length=50, blank=True)
    content_uri = models.CharField(max_length=200, blank=True)
    version = models.IntegerField(blank=False)
    content = models.TextField(blank=False, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    license_type = models.CharField(max_length=50, blank=True)


    def __str__(self):
        return str(self.id)

    class Meta:
        managed = True

class License(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50, blank=False)
    description = models.TextField(blank=True)
    content_uri = models.CharField(max_length=200, blank=True)
    unique_name = models.CharField(max_length=50, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)

    class Meta:
        managed = True


