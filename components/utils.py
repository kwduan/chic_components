__author__ = 'me1kd'

from .models import Component, Profile, Family, License
from .serializer import ComponentSerializer, ProfileSerializer, FamilySerializer, LicenseSerializer
from CHIC_components import settings


def getComponents():
    """ Get all the components from db

    :return: components in list of orderedDict
    """
    model = Component.objects.all()
    serializer = ComponentSerializer(model, many=True)
    return serializer.data

def getComponent(ID):
    """

    :param ID: The ID used to filter
    :return: one orderedDict
    """
    model = Component.objects.get(id=ID)
    serializer = ComponentSerializer(model)
    return serializer.data

def createURIforComponentContent(id):
    s_id = str(id)
    tmp_model = Component.objects.get(id=id)
    filename = tmp_model.title
    file_name = filename + '.t2flow'
    url = settings.HOSTNAME + '/files/'+ s_id + '/download/' + file_name
    #To-do: Add real function to generate URL.
    return url

def getProfiles():
    """ Get all the profiles from db

    :return: components in list of orderedDict
    """
    model = Profile.objects.all()
    serializer = ProfileSerializer(model, many=True)
    return serializer.data

def getProfile(ID):
    """

    :param ID: The ID used to filter
    :return: one orderedDict
    """
    model = Profile.objects.get(id=ID)
    serializer = ProfileSerializer(model)
    return serializer.data

def createURIforProfileContent(id):
    s_id = str(id)
    tmp_model = Profile.objects.get(id=id)
    filename = tmp_model.filename
    filename = filename.replace(' ','')
    url = settings.HOSTNAME + '/files/'+ s_id + '/download/' + filename
    return url

def getFamilies():
    """ Get all the profiles from db

    :return: components in list of orderedDict
    """
    model = Family.objects.all()
    serializer = FamilySerializer(model, many=True)
    return serializer.data

def getFamily(ID):
    """

    :param ID: The ID used to filter
    :return: one orderedDict
    """
    model = Family.objects.get(id=ID)
    serializer = FamilySerializer(model)
    return serializer.data

def createURIforFamilyContent(id):
    s_id = str(id)
    url = settings.HOSTNAME + '/component-family.xml?id=' + s_id
    #To-do: Add real function to generate URL.
    return url

def getLicenses():
    model = License.objects.all()
    serializer = LicenseSerializer(model, many=True)
    return serializer.data

def getLicense(ID):
    """

    :param ID: The ID used to filter
    :return: one orderedDict
    """
    model = License.objects.get(id=ID)
    serializer = LicenseSerializer(model)
    return serializer.data

def createURIforLicenseContent(id):
    s_id = str(id)
    url = settings.HOSTNAME + '/license.xml?id=' + s_id
    #To-do: Add real function to generate URL.
    return url

