__author__ = 'me1kd'

from . import models, serializer, utils
import xmltodict


def component_parser(id, xml_string):
    d_component = xmltodict.parse(xml_string)
    d_output = dict()
    d_output['title'] = d_component['workflow']['title'] if d_component['workflow']['title'] else ''
    d_output['description'] = d_component['workflow']['description'] if d_component['workflow']['description']!=None else ' '
    d_output['component_family'] = d_component['workflow']['component-family'] if d_component['workflow'][
        'component-family'] else ''
    d_output['license_type'] = d_component['workflow']['license-type'] if d_component['workflow'][
        'license-type'] else ''
    d_output['content_type'] = d_component['workflow']['content-type'] if d_component['workflow'][
        'content-type'] else ''
    d_output['content'] = d_component['workflow']['content']['#text'] if d_component['workflow']['content']['#text'] else ''
    if id:
        tmp_model = models.Component.objects.get(id=id)
        tmp_serializer = serializer.ComponentSerializer(tmp_model)
        d_output['version'] = tmp_serializer['version'] + 1
    else:
        d_output['version'] = 1
    return d_output


def profile_parser(id, xml_string):
    d_profile = xmltodict.parse(xml_string)
    d_output = dict()
    for key in d_profile['file'].keys():
        new_key = key.replace('-', '_')
        d_output[new_key] = d_profile['file'][key]
    d_output['content'] = d_profile['file']['content']['#text']
    if id:
        tmp_model = models.Profile.objects.get(id=id)
        tmp_serializer = serializer.ComponentSerializer(tmp_model)
        d_output['version'] = tmp_serializer['version'] + 1
    else:
        d_output['version'] = 1
    return d_output


def family_parser(id, xml_string):
    d_family = xmltodict.parse(xml_string)
    d_output = dict()
    d_output['title'] = d_family['pack']['title']
    d_output['description'] = d_family['pack']['description']
    d_output['license_type'] = d_family['pack']['license-type']
    d_output['component_profile'] = d_family['pack']['component-profile'] if d_family['pack']['component-profile'] else ''
    return d_output


def license_parser(id, xml_string):
    d_license = xmltodict.parse(xml_string)
    d_output = dict()
    d_output['title'] = d_license['license']['title']
    d_output['description'] = d_license['license']['description']
    d_output['unique_name'] = d_license['license']['unique-name']
    d_output['content_url'] = d_license['license']['uri']
    return d_output