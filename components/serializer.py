__author__ = 'me1kd'

from . import models
from rest_framework import serializers


class ComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Component

class FamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Family

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Profile

class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.License