from django.test import TestCase
from .models import Component
from .serializer import ComponentSerializer
import render


class ComponentTestCase(TestCase):
    def setUp(self):
        Component.objects.create(title='test', description='test', content_uri='test', version=1)


    def getComponent(self):
        test_model = Component.objects.get(id=1)
        print(test_model)
        serializer = ComponentSerializer(test_model)
        print(serializer.data)
        return serializer.data

    def getParsedXML(self):
        render.component_render(1)

    def test_component(self):
        #self.setUp()
        data = self.getComponent()
        self.getParsedXML()
        print(data['id'])
        self.assertEqual(data['title'], u'test')