__author__ = 'me1kd'

from django.conf.urls import include, url, patterns
from . import views

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'CHIC_components.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^components.xml$', views.Components.as_view()),
                       url(r'^component.xml$', views.Component.as_view()),
                       url(r'^component-profiles.xml$', views.ComponentProfiles.as_view()),
                       url(r'^component-profile.xml', views.ComponentProfile.as_view()),
                       url(r'^component-families.xml', views.ComponentFamilies.as_view()),
                       url(r'^component-family.xml', views.ComponentFamily.as_view()),
                       url(r'^licenses.xml', views.Licenses.as_view()),
                       url(r'^license.xml', views.License.as_view()),
                       url(r'^whoami.xml', views.Whoami.as_view()),
                       url(r'^workflow.xml', views.workflow.as_view()),
                       url(r'^files/(?P<id>[0-9]+)/download/(?P<file_name>\w+).(?P<extension_name>\w+)$', views.FileStorage.as_view())
                       )