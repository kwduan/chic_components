from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_swagger import urls as docs_urls
from components import urls as components_urls

urlpatterns = patterns('',
                       url(r'^', include(components_urls)),
                       url(r'^docs/', include(docs_urls)),
                       url(r'^admin/', include(admin.site.urls)),
                       )

