from django.contrib import admin

from .models import Component, Profile, Family, License

admin.site.register(Component)
admin.site.register(Profile)
admin.site.register(Family)
admin.site.register(License)