from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . import render, parser, serializer, utils, models
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
import base64, urllib, os
from rest_framework.renderers import JSONRenderer

# The APIs to list all Components
class Components(APIView):
    def get(self, request):
        component_family = request.GET.get('component-family', '')
        elements = request.GET.get('elements', '')
        if not component_family:
            if not elements:
                xml_resp = render.components_render()
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
            else:
                elements = urllib.unquote(elements)
                l_elements = elements.split(",")
                xml_resp = render.components_render_elements(l_elements)
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
        else:
            elements = urllib.unquote(elements)
            l_elements = elements.split(",")
            xml_resp = render.components_render_elements_profile(l_elements, component_family)
            if xml_resp:
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
            else:
                #error_message = '<error code="404" message="Not Found"><reason>Component family not found</reason></error>'
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')

# The APIs to CRUD Component
class Component(APIView):
    def get(self, request):
        """
        get component by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer

        """
        id = request.GET.get('id', '')
        if not id:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            xml_resp = render.component_render(id)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')


    def post(self, request):
        """
        post component in xml
        ---
        parameters:
            - name: xml
              required: true
              paramType: body
        """
        id = request.GET.get('id', '')
        if not id:
            xml_string = request.body
            d_xml = parser.component_parser(None, xml_string)
            tmp_serializer = serializer.ComponentSerializer(data=d_xml)
            if tmp_serializer.is_valid():
                tmp_serializer.save()
                tmp_component = tmp_serializer.data
                tmp_content_url = utils.createURIforComponentContent(tmp_component['id'])
                id = tmp_component['id']
                tmp_model = models.Component.objects.get(id=id)
                tmp_model.content_uri = tmp_content_url
                tmp_model.save()
                xml_resp = render.component_render(id)
                return Response(status=status.HTTP_200_OK, content_type='application/xml')
            return Response(tmp_serializer.error, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        """
        delete component by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer
        """
        id = request.GET.get('id', '')
        if id:
            tmp_model = models.Component.objects.get(id=id)
            tmp_model.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


# The APIs to list all Families
class ComponentFamilies(APIView):
    def get(self, request):
        elements = request.GET.get('elements', '')
        component_profile = request.GET.get('component-profile', '')
        print component_profile
        if not component_profile:
            if not elements:
                xml_resp = render.families_render()
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
            else:
                elements = urllib.unquote(elements)
                l_elements = elements.split(",")
                xml_resp = render.families_render_elements(l_elements)
                return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
        else:
            elements = urllib.unquote(elements)
            l_elements = elements.split(",")
            xml_resp = render.families_render_elements_profile(l_elements, component_profile)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')


# The APIs to CRUD Component Family
class ComponentFamily(APIView):
    def get(self, request):
        """
        get family by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer

        """
        id = request.GET.get('id', '')
        if not id:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            xml_resp = render.family_render(id)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')

    def post(self, request):
        """
        post family in xml
        ---
        parameters:
            - name: xml
              required: true
              paramType: body
        """
        id = request.GET.get('id', '')
        if not id:
            xml_string = request.body
            print xml_string
            d_xml = parser.family_parser(None, xml_string)
            tmp_serializer = serializer.FamilySerializer(data=d_xml)
            if tmp_serializer.is_valid():
                tmp_serializer.save()
                tmp_family = tmp_serializer.data
                tmp_content_url = utils.createURIforFamilyContent(tmp_family['id'])
                tmp_model = models.Family.objects.get(id=tmp_family['id'])
                tmp_model.content_uri = tmp_content_url
                tmp_model.save()
                xml_result = render.family_render(tmp_family['id'])
                print xml_result
                return HttpResponse(xml_result, status=status.HTTP_200_OK, content_type='application/xml')
            return Response(tmp_serializer.error, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        """
        delete family by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer
        """
        id = request.GET.get('id', '')
        if id:
            tmp_model = models.Family.objects.get(id=id)
            tmp_model.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


# The APIs to list all Profiles
class ComponentProfiles(APIView):
    def get(self, request):
        elements = request.GET.get('elements', '')
        if not elements:
            xml_resp = render.profiles_render()
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
        else:
            elements = urllib.unquote(elements)
            l_elements = elements.split(",")
            xml_resp = render.profiles_render_elements(l_elements)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')


# The APIs to CRUD Profile
class ComponentProfile(APIView):
    def get(self, request):
        """
        get profile by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer

        """
        id = request.GET.get('id', '')
        if not id:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            xml_resp = render.profile_render(id)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')

    def post(self, request):
        """
        post profile in xml
        ---
        parameters:
            - name: xml
              required: true
              paramType: body
        """
        id = request.GET.get('id', '')
        if not id:
            xml_string = request.body
            print xml_string
            d_xml = parser.profile_parser(None, xml_string)
            tmp_serializer = serializer.ProfileSerializer(data=d_xml)
            if tmp_serializer.is_valid():
                tmp_serializer.save()
                tmp_profile = tmp_serializer.data
                tmp_content_url = utils.createURIforProfileContent(tmp_profile['id'])
                tmp_model = models.Profile.objects.get(id=tmp_profile['id'])
                tmp_model.content_uri = tmp_content_url
                tmp_model.save()
                xml_result = render.profile_render(tmp_profile['id'])
                return HttpResponse(xml_result, status=status.HTTP_200_OK)
            return Response(tmp_serializer.error, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        """
        delete profile by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer
        """
        id = request.GET.get('id', '')
        if id:
            tmp_model = models.Profile.objects.get(id=id)
            tmp_model.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class FileStorage(APIView):
    renderer_classes = (JSONRenderer, )

    def get(self, request, id, file_name, extension_name):
        if id:
            if extension_name == 'xml':
                tmp_model = models.Profile.objects.get(id=id)
                tmp_serializer = serializer.ProfileSerializer(tmp_model)
                content = tmp_serializer['content'].value
                filename = file_name + '.xml'
                binary_stream = base64.decodestring(content)
                resp = HttpResponse(binary_stream, content_type='application/vnd.taverna.component-profile+xml')
                resp['Content-Disposition'] = 'attachment; filename="' + filename + '"'
                return resp
            elif extension_name == 't2flow':
                tmp_model = models.Component.objects.get(id=id)
                tmp_serializer = serializer.ComponentSerializer(tmp_model)
                content = tmp_serializer['content'].value
                filename = file_name + '.t2flow'
                binary_stream = base64.decodestring(content)
                resp = HttpResponse(binary_stream, content_type='application/vnd.taverna.t2flow+xml')
                resp['Content-Disposition'] = 'attachment; filename="' + filename + '"'
                return resp
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class Licenses(APIView):
    def get(self, request):
        elements = request.GET.get('elements', '')
        if not elements:
            xml_resp = render.licenses_render()
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')
        else:
            elements = urllib.unquote(elements)
            l_elements = elements.split(",")
            xml_resp = render.licenses_render_elements(l_elements)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')


class License(APIView):
    def get(self, request):
        """
        get license by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer

        """
        id = request.GET.get('id', '')
        if not id:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            xml_resp = render.license_render(id)
            return HttpResponse(xml_resp, status=status.HTTP_200_OK, content_type='application/xml')

    def post(self, request):
        """
        post profile in xml
        ---
        parameters:
            - name: xml
              required: true
              paramType: body
        """
        id = request.GET.get('id', '')
        if not id:
            xml_string = request.body
            d_xml = parser.license_parser(None, xml_string)
            tmp_serializer = serializer.ProfileSerializer(data=d_xml)
            if tmp_serializer.is_valid():
                tmp_serializer.save()
                return Response(status=status.HTTP_200_OK)
            return Response(tmp_serializer.error, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        """
        delete profile by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer
        """
        id = request.GET.get('id', '')
        if id:
            tmp_model = models.License.objects.get(id=id)
            tmp_model.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class Whoami(APIView):
    def get(self, request):
        module_dir = module_dir = os.path.dirname(__file__)
        file_path = os.path.join(module_dir, 'resources/whoami.xml')
        with open(file_path, "r") as xml_file:
            data = xml_file.read()
        resp = HttpResponse(data, content_type='application/vnd.taverna.component-profile+xml')
        resp['Content-Disposition'] = 'attachment; filename="whoami.xml"'
        return resp


class workflow(APIView):
    def delete(self, request):
        """
        delete component by id
        ---
        parameters:
            - name: id
              required: true
              paramType: query
              type: integer
        """
        # return Response(status=status.HTTP_200_OK)
        id = request.GET.get('id', '')
        if id != 'null':
            tmp_model = models.Family.objects.get(id=id)
            tmp_model.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
