__author__ = 'me1kd'

from . import utils
import xmltodict


#region component
def components_render():
    lod_component = utils.getComponents()
    dict_output = dict()
    dict_sub_output = dict()
    workflows = []
    for item in lod_component:
        d_workflow_output = dict()
        d_workflow_output['@id'] = str(item['id'])
        d_workflow_output['@resource'] = ''
        d_workflow_output['@uri'] = item['content_uri']
        d_workflow_output['#text'] = item['description']
        d_workflow_output['@version'] = str(item['version'])
        workflows.append(d_workflow_output)
    dict_sub_output['workflow'] = workflows
    dict_output['workflows'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def components_render_elements(l_elements):
    lod_component = utils.getComponents()
    dict_output = dict()
    dict_sub_output = dict()
    workflows = []
    for item in lod_component:
        d_workflow_output = dict()
        d_workflow_output['@id'] = str(item['id'])
        d_workflow_output['@resource'] = ''
        d_workflow_output['@uri'] = item['content_uri']
        d_workflow_output['@version'] = str(item['version'])
        for element in l_elements:
            element_r = element.replace('-','_')
            d_workflow_output[element_r] = item[element]
        workflows.append(d_workflow_output)
    dict_sub_output['workflow'] = workflows
    dict_output['workflows'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output


def component_render(ID):
    od_component = utils.getComponent(ID)
    tmp_component = dict()
    tmp_component['@uri'] = od_component['content_uri']
    tmp_component['@version'] = str(od_component['version'])
    tmp_component['@resource'] = od_component['content_uri']
    tmp_component['id'] = str(od_component['id'])
    tmp_component['title'] = od_component['title']
    tmp_component['content-uri'] = od_component['content_uri']
    tmp_component['description'] = od_component['description']
    tmp_component['version'] = str(od_component['version'])
    tmp_component['content-type'] = od_component['content_type']
    tmp_component['license-type'] = od_component['license_type']
    tmp_component['component-family'] = od_component['component_family']
    tmp_component['created-at'] = od_component['created_at']
    dict_output = dict()
    dict_output['workflow'] = tmp_component
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def components_render_elements_profile(l_elements, family):
    lod_component = utils.getComponents()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    empty = True
    for item in lod_component:
        if item['component_family'] == family:
            empty = False
            d_family_output = dict()
            d_family_output['@id'] = str(item['id'])
            d_family_output['@resource'] = ''
            d_family_output['@uri'] = item['content_uri']
            d_family_output['@version'] = item['version']
            for element in l_elements:
                element_r = element.replace('-','_')
                d_family_output[element] = item[element_r]
            profiles.append(d_family_output)
        else:
            continue
    dict_sub_output['workflow'] = profiles
    dict_output['workflows'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output
#endregion

#region family
def families_render():
    lod_family = utils.getFamilies()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    for item in lod_family:
        d_family_output = dict()
        d_family_output['@id'] = str(item['id'])
        d_family_output['@resource'] = ''
        d_family_output['@uri'] = item['content_uri']
        d_family_output['#text'] = item['title']
        d_family_output['@version'] = ''
        profiles.append(d_family_output)
    dict_sub_output['pack'] = profiles
    dict_output['component-profiles'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def families_render_elements(l_elements):
    lod_family = utils.getFamilies()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    for item in lod_family:
        d_family_output = dict()
        d_family_output['@id'] = str(item['id'])
        d_family_output['@resource'] = ''
        d_family_output['@uri'] = item['content_uri']
        d_family_output['@version'] = ''
        for element in l_elements:
            element_r = element.replace('-','_')
            d_family_output[element] = item[element_r]
        profiles.append(d_family_output)
    dict_sub_output['pack'] = profiles
    dict_output['component-families'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def families_render_elements_profile(l_elements, profile):
    lod_family = utils.getFamilies()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    for item in lod_family:
        if item['component_profile'] == profile:
            d_family_output = dict()
            d_family_output['@id'] = str(item['id'])
            d_family_output['@resource'] = ''
            d_family_output['@uri'] = item['content_uri']
            d_family_output['@version'] = ''
            for element in l_elements:
                element_r = element.replace('-','_')
                d_family_output[element] = item[element_r]
            profiles.append(d_family_output)
        else:
            continue
    dict_sub_output['pack'] = profiles
    dict_output['component-families'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def family_render(ID):
    od_family = utils.getFamily(ID)
    tmp_family = dict()
    tmp_family['@uri'] = od_family['content_uri']
    tmp_family['@resource'] = od_family['content_uri']
    tmp_family['id'] = str(od_family['id'])
    tmp_family['title'] = od_family['title']
    tmp_family['description'] = od_family['description']
    tmp_family['component-profile'] = od_family['component_profile']
    tmp_family['license-type'] = od_family['license_type']
    tmp_family['created-at'] = od_family['created_at']
    dict_output = dict()
    dict_output['pack'] = tmp_family
    xml_output = xmltodict.unparse(dict_output)
    return xml_output
#endregion

#region profile
def profiles_render():
    lod_profile = utils.getProfiles()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    for item in lod_profile:
        d_profile_output = dict()
        d_profile_output['@id'] = str(item['id'])
        d_profile_output['@resource'] = ''
        d_profile_output['@uri'] = item['content_uri']
        d_profile_output['#text'] = item['title']
        d_profile_output['@version'] = str(item['version'])
        profiles.append(d_profile_output)
    dict_sub_output['file'] = profiles
    dict_output['component-profiles'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def profiles_render_elements(l_elements):
    lod_profile = utils.getProfiles()
    dict_output = dict()
    dict_sub_output = dict()
    profiles = []
    for item in lod_profile:
        d_profile_output = dict()
        d_profile_output['@id'] = str(item['id'])
        d_profile_output['@resource'] = item['content_uri']
        d_profile_output['@uri'] = item['content_uri']
        d_profile_output['@version'] = str(item['version'])
        for element in l_elements:
            element_r = element.replace('-','_')
            d_profile_output[element] = item[element_r]
        profiles.append(d_profile_output)
    dict_sub_output['file'] = profiles
    dict_output['component-profiles'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def profile_render(ID):
    od_profile = utils.getProfile(ID)
    tmp_profile = dict()
    tmp_profile['@resource'] = utils.createURIforProfileContent(od_profile['id'])
    tmp_profile['@uri'] = utils.createURIforProfileContent(od_profile['id'])
    tmp_profile['id'] = str(od_profile['id'])
    tmp_profile['filename'] = od_profile['filename']
    tmp_profile['content-uri'] = od_profile['content_uri']
    tmp_profile['description'] = od_profile['description']
    tmp_profile['version'] = str(od_profile['version'])
    tmp_profile['content-type'] = od_profile['content_type']
    tmp_profile['license-type'] = od_profile['license_type']
    tmp_profile['created-at'] = od_profile['created_at']
    tmp_profile['title']=od_profile['title']
    dict_output = dict()
    dict_output['file'] = tmp_profile
    xml_output = xmltodict.unparse(dict_output)
    return xml_output
#endregion

#region license
def licenses_render():
    lod_license = utils.getLicenses()
    dict_output = dict()
    dict_sub_output = dict()
    licenses = []
    for item in lod_license:
        d_license_output = dict()
        d_license_output['@id'] = str(item['id'])
        d_license_output['@resource'] = ''
        d_license_output['@uri'] = utils.createURIforFamilyContent(str(item['id']))
        d_license_output['#text'] = item['title']
        licenses.append(d_license_output)
    dict_sub_output['license'] = licenses
    dict_output['licenses'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def licenses_render_elements(l_elements):
    lod_license = utils.getLicenses()
    dict_output = dict()
    dict_sub_output = dict()
    licenses = []
    for item in lod_license:
        d_license_output = dict()
        d_license_output['@id'] = str(item['id'])
        d_license_output['@resource'] = ''
        d_license_output['@uri'] = utils.createURIforFamilyContent(str(item['id']))
        licenses.append(d_license_output)
        for element in l_elements:
            element_r = element.replace('-','_')
            d_license_output[element] = item[element_r]
        licenses.append(d_license_output)
    dict_sub_output['license'] = licenses
    dict_output['licenses'] = dict_sub_output
    xml_output = xmltodict.unparse(dict_output)
    return xml_output

def license_render(ID):
    od_license = utils.getLicense(ID)
    tmp_license = dict()
    tmp_license['id'] = str(od_license['id'])
    tmp_license['unique-name'] = od_license['unique_name']
    tmp_license['title'] = od_license['title']
    tmp_license['uri'] = od_license['content_uri']
    tmp_license['description'] = od_license['description']
    tmp_license['created-at'] = od_license['created_at']
    dict_output = dict()
    dict_output['license'] = tmp_license
    xml_output = xmltodict.unparse(dict_output)
    return xml_output
#endregion
